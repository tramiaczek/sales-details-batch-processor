package pl.tramiaczek.sales.control;

import com.opencsv.CSVReader;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.*;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.context.annotation.Scope;
import pl.tramiaczek.sales.entity.SaleDetail;

import java.io.IOException;
import java.io.Reader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;

@Scope("step")
public class SalesDetailsReader implements ItemReader<SaleDetail>, StepExecutionListener {

	private static final Log LOG = LogFactory.getLog(SalesDetailsReader.class);

	private CSVReader csvReader;

	private SalesDetailsReader() {
	}

	public static SalesDetailsReader create() {
		return new SalesDetailsReader();
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		final String filePathToProcess = getFilePathForProcessing(stepExecution);

		if (LOG.isDebugEnabled()) {
			LOG.debug(String.format("Initialize CSV reader for file: %s", filePathToProcess));
		}

		try {
			Reader reader = Files.newBufferedReader(Paths.get(filePathToProcess));
			this.csvReader = new CSVReader(reader);
			this.csvReader.readNext(); //skip firs line as it is a header
		} catch (IOException e) {
			LOG.warn(String.format("Problem while creating the reader for file: %s", filePathToProcess), e);
		}
	}

	private String getFilePathForProcessing(StepExecution stepExecution) {
		final JobParameters parameters = stepExecution.getJobExecution().getJobParameters();

		String filePathForProcessing = parameters.getString(SalesDetailsBatchProcessorConfiguration.FILE_TO_PARSE_JOB_PARAMETER);
		if (filePathForProcessing == null) {
			throw new IllegalStateException("No parameter: "
					+ SalesDetailsBatchProcessorConfiguration.FILE_TO_PARSE_JOB_PARAMETER + " in job configuration, nothing to parse...");
		}

		return filePathForProcessing;
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		final String filePathToProcess = getFilePathForProcessing(stepExecution);

		if (this.csvReader != null) {
			try {
				this.csvReader.close();
			} catch (IOException e){
				LOG.warn(String.format("Problem while closing CSV reader for file: %s. Exception details: ", filePathToProcess), e);
			}
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug(String.format("Closed CSV reader for file: %s", filePathToProcess));
		}
		return ExitStatus.COMPLETED;
	}

	@Override
	public SaleDetail read() throws IOException, UnexpectedInputException, ParseException, java.text.ParseException, NonTransientResourceException {

		if (csvReader != null) {
			String[] csvRecord = this.csvReader.readNext();
			if (csvRecord != null && csvRecord.length == 14) {
				String DATE_FORMAT = "MM/dd/yyyy";
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
				if (LOG.isDebugEnabled()) {
					LOG.debug(String.format("Sales detail read for ID: %s", csvRecord[6]));
				}
				return SaleDetail.create(
						Long.valueOf(csvRecord[6]), csvRecord[0].intern(),
						csvRecord[1].intern(), csvRecord[2].intern(), csvRecord[3].intern(), csvRecord[4].intern(),
						simpleDateFormat.parse(csvRecord[5]), simpleDateFormat.parse(csvRecord[7]),
						new BigDecimal(csvRecord[8]), new BigDecimal(csvRecord[9]), new BigDecimal(csvRecord[10]),
						new BigDecimal(csvRecord[11]), new BigDecimal(csvRecord[12]), new BigDecimal(csvRecord[13])
				);
			}
		}

		return null;
	}
}
