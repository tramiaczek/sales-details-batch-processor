package pl.tramiaczek.sales.control;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemWriter;
import org.springframework.context.annotation.Scope;
import pl.tramiaczek.sales.entity.SaleDetail;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.transaction.Transaction;
import java.util.List;

@Scope("step")
public class SalesDetailsWriter implements ItemWriter<SaleDetail>, StepExecutionListener {

	private static final Log LOG = LogFactory.getLog(SalesDetailsWriter.class);

	private SalesDetailsDao salesDetailsDao;

	private SalesDetailsWriter(SalesDetailsDao salesDetailsDao) {
		this.salesDetailsDao = salesDetailsDao;
	}

	static SalesDetailsWriter create(SalesDetailsDao salesDetailsDao) {
		return new SalesDetailsWriter(salesDetailsDao);
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Start writing sales details entities to database...");
		}
	}


	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Sales details entities have been written to database...");
		}
		return ExitStatus.COMPLETED;
	}

	@Override
	public void write(List<? extends SaleDetail> list) {
		if (list != null) {
			EntityTransaction transaction = this.salesDetailsDao.getTransaction();
			try {
				transaction.begin();
				this.salesDetailsDao.persist(list);
				transaction.commit();
			} finally {
				this.salesDetailsDao.clearContext();
			}

			LOG.info(String.format("Number of record in database: %s, occupied memory after chunk write: %s MB",
					this.salesDetailsDao.getSalesDetailsRowsNumber(),
					(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024*1024)));
		}
	}
}
