package pl.tramiaczek.sales.control;

import org.jooq.SQLDialect;
import org.jooq.impl.DataSourceConnectionProvider;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultDSLContext;
import org.jooq.impl.DefaultExecuteListenerProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@ComponentScan("pl.tramiaczek.sales.jooq")
@Profile("jooq")
public class SalesDetailsJOOQConfiguration {

	@Value("${jooq.sql.dialect}")
	private String jooqDialect;

	@Value("${spring.datasource.hikari.jdbc-url}")
	private String dbUrl;

	@Value("${spring.datasource.hikari.username}")
	private String dbUser;

	@Value("${spring.datasource.hikari.password}")
	private String dbPassword;

	@Value("${spring.datasource.hikari.driver-class-name}")
	private String dbDriverClassName;

	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setUrl(this.dbUrl);
		dataSource.setUsername(this.dbUser);
		dataSource.setPassword(this.dbPassword);
		dataSource.setDriverClassName(this.dbDriverClassName);
		return dataSource;
	}

	@Bean
	public TransactionAwareDataSourceProxy transactionAwareDataSource() {
		return new TransactionAwareDataSourceProxy(dataSource());
	}

	@Bean
	public DataSourceTransactionManager transactionManager() {
		return new DataSourceTransactionManager(dataSource());
	}

	@Bean
	public DataSourceConnectionProvider connectionProvider() {
		return new DataSourceConnectionProvider(transactionAwareDataSource());
	}

	@Bean
	public JOOQExceptionTranslator exceptionTransformer() {
		return new JOOQExceptionTranslator();
	}

	@Bean
	public DefaultConfiguration configuration() {
		DefaultConfiguration jooqConfiguration = new DefaultConfiguration();
		jooqConfiguration.set(connectionProvider());
		jooqConfiguration.set(new DefaultExecuteListenerProvider(exceptionTransformer()));

		SQLDialect dialect = SQLDialect.valueOf(this.jooqDialect);
		jooqConfiguration.set(dialect);

		return jooqConfiguration;
	}

	@Bean
	public DefaultDSLContext dsl() {
		return new DefaultDSLContext(configuration());
	}

}
