package pl.tramiaczek.sales.control;

import org.springframework.batch.core.ItemWriteListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import pl.tramiaczek.sales.entity.SaleDetail;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

@Configuration
@EnableBatchProcessing
public class SalesDetailsBatchProcessorConfiguration {

	public static final String FILE_TO_PARSE_JOB_PARAMETER = "FILE_TO_PARSE";

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Value("${processor.chunk.size}")
	private int chunkSize;

	@Bean
	JobExecutionListener jobExecutionListener(SalesDetailsDao salesDetailsDao) {
		return SalesDetailsJobExecutionListenerImpl.create(salesDetailsDao);
	}

	@Bean
	ItemWriteListener<SaleDetail> itemWriteListener() {
		return SalesDetailsItemWriteListenerImpl.create();
	}

	@Bean
	public ItemReader<SaleDetail> salesDetailsReader() {
		return SalesDetailsReader.create();
	}

	@Bean
	@Profile("only.csv.reading")
	public ItemWriter<SaleDetail> dummyWriter() {
		return DummyWriter.create();
	}

	@Bean
	@ConditionalOnMissingBean(ItemWriter.class)
	public ItemWriter<SaleDetail> salesDetailsWriter(SalesDetailsDao salesDetailsDao) {
		return SalesDetailsWriter.create(salesDetailsDao);
	}

	@Bean
	protected Step processSalesDetails(ItemReader<SaleDetail> reader, ItemWriter<SaleDetail> writer) {
		return stepBuilderFactory.get("processSalesDetails").<SaleDetail, SaleDetail> chunk(this.chunkSize)
				.reader(reader)
				.writer(writer)
				.listener(itemWriteListener())
				.build();
	}

	@Bean
	@Qualifier("salesDetailsJob")
	public Job salesDetailsJob(ItemReader<SaleDetail> reader, ItemWriter<SaleDetail> writer, SalesDetailsDao salesDetailsDao) {
		return jobBuilderFactory
				.get("salesDetailsChunkJob")
				.start(processSalesDetails(reader, writer))
				.listener(jobExecutionListener(salesDetailsDao))
				.build();
	}
}
