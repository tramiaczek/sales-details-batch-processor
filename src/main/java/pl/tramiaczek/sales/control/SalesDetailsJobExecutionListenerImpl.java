package pl.tramiaczek.sales.control;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.stereotype.Component;

@Component
public class SalesDetailsJobExecutionListenerImpl implements JobExecutionListener {

	private static final Log LOG = LogFactory.getLog(SalesDetailsJobExecutionListenerImpl.class);

	private SalesDetailsDao salesDetailsDao;

	private SalesDetailsJobExecutionListenerImpl(SalesDetailsDao salesDetailsDao) {
		this.salesDetailsDao = salesDetailsDao;
	}

	static SalesDetailsJobExecutionListenerImpl create(SalesDetailsDao salesDetailsDao) {
		return new SalesDetailsJobExecutionListenerImpl(salesDetailsDao);
	}

	@Override
	public void beforeJob(JobExecution jobExecution) {
		LOG.info(String.format("Job: %s started at: %s. Occupied memory before job start: %s MB",
				jobExecution.getJobInstance().getInstanceId(), jobExecution.getStartTime(),
				(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024*1024) + " MB"));
	}

	@Override
	public void afterJob(JobExecution jobExecution) {
		LOG.info(String.format(
				"Job: %s finished at: %s. %d records have been inserted. Occupied memory after job finish: %s MB",
				jobExecution.getJobConfigurationName(), jobExecution.getEndTime(),
				this.salesDetailsDao.getSalesDetailsRowsNumber(), ((Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / (1024*1024))));
	}
}
