package pl.tramiaczek.sales.control;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import pl.tramiaczek.sales.entity.SaleDetail;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Repository
@Profile("jpa")
public class SalesDetailsJpaDaoImpl implements SalesDetailsDao {

	@PersistenceUnit
	private EntityManagerFactory entityManagerFactory;

	private EntityManager entityManager;

	public SalesDetailsJpaDaoImpl() {
	}

	@PostConstruct
	private void initialize() {
		if (this.entityManager == null || !this.entityManager.isOpen()) {
			this.entityManager = this.entityManagerFactory.createEntityManager();
		}
	}

	@Override
	public EntityTransaction getTransaction() {
		initialize();
		return this.entityManager.getTransaction();
	}

	@Override
	public void persist(List<? extends SaleDetail> objects) {
		initialize();
		objects.forEach(sd -> {
			this.entityManager.persist(sd);
			sd = null;
		});
	}

	@Override
	public void clearContext() {
		if (entityManager.isOpen()) {
			entityManager.clear();
		}
	}

	@Transactional
	@Override
	public long getSalesDetailsRowsNumber() {
		initialize();
		long savedRecords = (Long) entityManager.createQuery("SELECT COUNT(sd) FROM pl.tramiaczek.sales.entity.SaleDetail sd").getSingleResult();
		entityManager.clear();
		entityManager.close();
		return savedRecords;
	}
}
