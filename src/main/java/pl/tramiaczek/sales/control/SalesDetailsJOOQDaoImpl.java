package pl.tramiaczek.sales.control;

import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.InsertQuery;
import org.jooq.impl.DSL;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import pl.tramiaczek.sales.entity.SaleDetail;
import pl.tramiaczek.sales.jooq.entity.Sequences;
import pl.tramiaczek.sales.jooq.entity.tables.records.SaleDetailRecord;

import javax.persistence.EntityTransaction;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
@Profile("jooq")
public class SalesDetailsJOOQDaoImpl implements SalesDetailsDao {

	private DSLContext context;
	private Configuration configuration;

	pl.tramiaczek.sales.jooq.entity.tables.SaleDetail dslSaleDetail = pl.tramiaczek.sales.jooq.entity.tables.SaleDetail.SALE_DETAIL;

	public SalesDetailsJOOQDaoImpl(DSLContext context, Configuration configuration) {
		this.context = context;
		this.configuration = configuration;
	}

	@Override
	public EntityTransaction getTransaction() {
		return new DummyEntityTransaction();
	}

	@Override
	public void persist(List<? extends SaleDetail> objects) {
		DSL.using(configuration)
				.transaction(c -> {
					context.batch(
							prepareQueries(objects)
					).execute();
				});
	}

	private Set<InsertQuery<SaleDetailRecord>> prepareQueries(List<? extends SaleDetail> objects) {
		return objects.stream().map(sd -> {
			InsertQuery<pl.tramiaczek.sales.jooq.entity.tables.records.SaleDetailRecord> query = context.insertQuery(dslSaleDetail);
			query.addValue(dslSaleDetail.ID, context.nextval(Sequences.SALE_DETAIL_ID_SEQ));
			query.addValue(dslSaleDetail.COUNTRY, sd.getCountry());
			query.addValue(dslSaleDetail.ITEM_TYPE, sd.getItemType());
			query.addValue(dslSaleDetail.ORDER_DATE, new java.sql.Date(sd.getOrderDate().getTime()));
			query.addValue(dslSaleDetail.ORDER_ID, sd.getOrderId());
			query.addValue(dslSaleDetail.ORDER_PRIORITY, sd.getOrderPriority());
			query.addValue(dslSaleDetail.REGION, sd.getRegion());
			query.addValue(dslSaleDetail.SALES_CHANNEL, sd.getSalesChannel());
			query.addValue(dslSaleDetail.SHIP_DATE, new java.sql.Timestamp(sd.getShipDate().getTime()));
			query.addValue(dslSaleDetail.TOTAL_COST, sd.getTotalCost());
			query.addValue(dslSaleDetail.TOTAL_PROFIT, sd.getTotalProfit());
			query.addValue(dslSaleDetail.TOTAL_COST, sd.getTotalCost());
			query.addValue(dslSaleDetail.TOTAL_REVENUE, sd.getTotalRevenue());
			query.addValue(dslSaleDetail.UNIT_COST, sd.getUnitCost());
			query.addValue(dslSaleDetail.UNIT_PRICE, sd.getUnitPrice());
			query.addValue(dslSaleDetail.UNITS_SOLD, sd.getUnitsSold());
			return query;
		}).collect(Collectors.toSet());
	}

	@Override
	public void clearContext() {

	}

	@Override
	public long getSalesDetailsRowsNumber() {
		return DSL.using(configuration)
						.selectCount()
						.from(pl.tramiaczek.sales.jooq.entity.tables.SaleDetail.SALE_DETAIL)
						.fetchOne(0, long.class);
	}

	private static class DummyEntityTransaction implements EntityTransaction {
		@Override
		public void begin() {

		}

		@Override
		public void commit() {

		}

		@Override
		public void rollback() {

		}

		@Override
		public void setRollbackOnly() {

		}

		@Override
		public boolean getRollbackOnly() {
			return false;
		}

		@Override
		public boolean isActive() {
			return false;
		}
	}
}
