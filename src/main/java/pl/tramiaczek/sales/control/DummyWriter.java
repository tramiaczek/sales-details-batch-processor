package pl.tramiaczek.sales.control;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemWriter;
import org.springframework.context.annotation.Scope;
import pl.tramiaczek.sales.entity.SaleDetail;

import java.util.List;

@Scope("step")
public class DummyWriter implements ItemWriter<SaleDetail>, StepExecutionListener {

	private static final Log LOG = LogFactory.getLog(DummyWriter.class);

	private DummyWriter() {
	}

	public static DummyWriter create() {
		return new DummyWriter();
	}

	@Override
	public void beforeStep(StepExecution stepExecution) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Start writing sales details entities to database...");
		}
	}


	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		if (LOG.isDebugEnabled()) {
			LOG.debug("Sales details entities have been written to database...");
		}
		return ExitStatus.COMPLETED;
	}

	@Override
	public void write(List<? extends SaleDetail> list) {
		//do nothing just for test memory during reading and parsing
	}
}
