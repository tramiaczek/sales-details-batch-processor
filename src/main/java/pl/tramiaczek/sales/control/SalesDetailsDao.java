package pl.tramiaczek.sales.control;

import pl.tramiaczek.sales.entity.SaleDetail;

import javax.persistence.EntityTransaction;
import java.util.List;

public interface SalesDetailsDao {
	EntityTransaction getTransaction();
	void persist(List<? extends SaleDetail> object);
	void clearContext();
	long getSalesDetailsRowsNumber();
}
