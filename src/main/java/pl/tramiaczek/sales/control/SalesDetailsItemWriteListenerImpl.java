package pl.tramiaczek.sales.control;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.ItemWriteListener;
import org.springframework.stereotype.Component;
import pl.tramiaczek.sales.entity.SaleDetail;

import java.util.List;

@Component
public class SalesDetailsItemWriteListenerImpl implements ItemWriteListener<SaleDetail> {

	private static final Log LOG = LogFactory.getLog(SalesDetailsItemWriteListenerImpl.class);

	private SalesDetailsItemWriteListenerImpl() {
	}

	static SalesDetailsItemWriteListenerImpl create() {
		return new SalesDetailsItemWriteListenerImpl();
	}

	@Override
	public void beforeWrite(List<? extends SaleDetail> list) {

	}

	@Override
	public void afterWrite(List<? extends SaleDetail> list) {
		if (list != null) {
			list.forEach(e -> e = null);
		}
	}

	@Override
	public void onWriteError(Exception e, List<? extends SaleDetail> list) {
		LOG.warn("Problem while writing list of objects", e);
	}
}
