package pl.tramiaczek.sales.control;

import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;
import pl.tramiaczek.sales.entity.SaleDetail;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Repository
@Profile("hibernate")
public class SalesDetailsHibernateDaoImpl implements SalesDetailsDao {

	@Autowired
	private SessionFactory sessionFactory;

	public SalesDetailsHibernateDaoImpl() {
	}

	@Override
	public EntityTransaction getTransaction() {
		return this.sessionFactory.getCurrentSession().getTransaction();
	}

	@Override
	public void persist(List<? extends SaleDetail> objects) {
		objects.forEach(sd -> {
			this.sessionFactory.getCurrentSession().save(sd);
			sd = null;
		});
	}

	@Override
	public void clearContext() {
	}

	@Override
	public long getSalesDetailsRowsNumber() {
		Transaction transaction = this.sessionFactory.getCurrentSession().beginTransaction();
		long savedRecords =
				(Long) this.sessionFactory.getCurrentSession().createQuery("SELECT COUNT(sd) FROM pl.tramiaczek.sales.entity.SaleDetail sd").getSingleResult();
		transaction.commit();
		return savedRecords;
	}
}
