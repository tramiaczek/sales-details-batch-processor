package pl.tramiaczek.sales.entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity(name = "SALE_DETAIL")
public class SaleDetail {

	@Column(name = "ID")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@SequenceGenerator(name = "sale_detail_id_seq")
	private long id;

	@Column(name = "ORDER_ID")
	private final long orderId;

	@Column(name = "REGION")
	private final String region;

	@Column(name = "COUNTRY")
	private final String country;

	@Column(name = "ITEM_TYPE")
	private final String itemType;

	@Column(name = "SALES_CHANNEL")
	private String salesChannel;

	@Column(name = "ORDER_PRIORITY")
	private final String orderPriority;

	@Column(name = "ORDER_DATE")
	@Temporal(TemporalType.DATE)
	private final Date orderDate;

	@Column(name = "SHIP_DATE")
	private final Date shipDate;

	@Column(name = "UNITS_SOLD")
	private final BigDecimal unitsSold;

	@Column(name = "UNIT_PRICE")
	private final BigDecimal unitPrice;

	@Column(name="UNIT_COST")
	private final BigDecimal unitCost;

	@Column(name="TOTAL_REVENUE")
	private final BigDecimal totalRevenue;

	@Column(name = "TOTAL_COST")
	private final BigDecimal totalCost;

	@Column(name = "TOTAL_PROFIT")
	private final BigDecimal totalProfit;

	public long getId() {
		return id;
	}

	public long getOrderId() {
		return orderId;
	}

	public String getRegion() {
		return region;
	}

	public String getCountry() {
		return country;
	}

	public String getItemType() {
		return itemType;
	}

	public String getSalesChannel() {
		return salesChannel;
	}

	public String getOrderPriority() {
		return orderPriority;
	}

	public Date getOrderDate() {
		return orderDate;
	}

	public Date getShipDate() {
		return shipDate;
	}

	public BigDecimal getUnitsSold() {
		return unitsSold;
	}

	public BigDecimal getUnitPrice() {
		return unitPrice;
	}

	public BigDecimal getUnitCost() {
		return unitCost;
	}

	public BigDecimal getTotalRevenue() {
		return totalRevenue;
	}

	public BigDecimal getTotalCost() {
		return totalCost;
	}

	public BigDecimal getTotalProfit() {
		return totalProfit;
	}

	private SaleDetail(long orderId, String region, String country, String itemType,
					   String salesChannel, String orderPriority, Date orderDate, Date shipDate, BigDecimal unitsSold,
					   BigDecimal unitPrice, BigDecimal unitCost, BigDecimal totalRevenue, BigDecimal totalCost, BigDecimal totalProfit) {
		this.orderId = orderId;
		this.region = region;
		this.country = country;
		this.itemType = itemType;
		this.salesChannel = salesChannel;
		this.orderPriority = orderPriority;
		this.orderDate = orderDate;
		this.shipDate = shipDate;
		this.unitsSold = unitsSold;
		this.unitPrice = unitPrice;
		this.unitCost = unitCost;
		this.totalRevenue = totalRevenue;
		this.totalCost = totalCost;
		this.totalProfit = totalProfit;
	}

	public static SaleDetail create(long orderId, String region, String country, String itemType,
									String salesChannel, String orderPriority, Date orderDate, Date shipDate, BigDecimal unitsSold,
									BigDecimal unitPrice, BigDecimal unitCost, BigDecimal totalRevenue, BigDecimal totalCost, BigDecimal totalProfit) {

		return new SaleDetail(orderId, region, country, itemType, salesChannel,
				orderPriority, orderDate, shipDate, unitsSold, unitPrice, unitCost, totalRevenue, totalCost, totalProfit);
	}

	@Override
	public String toString() {
		return "SaleDetail{" +
				"orderId=" + orderId +
				", region='" + region + '\'' +
				", country='" + country + '\'' +
				", itemType='" + itemType + '\'' +
				", salesChannel='" + salesChannel + '\'' +
				", orderPriority='" + orderPriority + '\'' +
				", orderDate=" + orderDate +
				", shipDate=" + shipDate +
				", unitsSold=" + unitsSold +
				", unitPrice=" + unitPrice +
				", unitCost=" + unitCost +
				", totalRevenue=" + totalRevenue +
				", totalCost=" + totalCost +
				", totalProfit=" + totalProfit +
				'}';
	}
}
