package pl.tramiaczek.sales.boundary;

import org.jooq.meta.derby.sys.Sys;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.tramiaczek.sales.control.SalesDetailsBatchProcessorConfiguration;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
public class SalesDetailsJobExecutorImpl implements SalesDetailsJobExecutor {

	@Autowired
	private JobLauncher jobLauncher;

	@Resource(name = "salesDetailsJob")
	private Job salesDetailsJob;

	@Override
	public void executeJob(String filePath) throws JobExecutionException {
		Map<String, JobParameter> mapTemplate = new HashMap<>();
		mapTemplate.put(SalesDetailsBatchProcessorConfiguration.FILE_TO_PARSE_JOB_PARAMETER,new JobParameter(filePath));
		mapTemplate.put("timestamp", new JobParameter(System.currentTimeMillis()));
		JobParameters jobParameters = new JobParameters(Collections.unmodifiableMap(mapTemplate));
		JobExecution jobExecution = this.jobLauncher.run(this.salesDetailsJob, jobParameters);
	}
}
