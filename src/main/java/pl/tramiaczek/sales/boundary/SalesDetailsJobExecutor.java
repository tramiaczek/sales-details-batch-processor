package pl.tramiaczek.sales.boundary;

import org.springframework.batch.core.JobExecutionException;

public interface SalesDetailsJobExecutor {
	void executeJob(String filePath) throws JobExecutionException;
}
