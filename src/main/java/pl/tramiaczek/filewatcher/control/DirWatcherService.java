package pl.tramiaczek.filewatcher.control;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.batch.core.JobExecutionException;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import pl.tramiaczek.sales.boundary.SalesDetailsJobExecutor;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.TimeUnit;

@Service
public class DirWatcherService {

	private static final Log LOG = LogFactory.getLog(DirWatcherService.class);

	@Value("${watcher.directory.path}")
	private String watchedDirPathValue;

	@Value("${watcher.poll.timeout}")
	private int watcherPollTimeout;

	private SalesDetailsJobExecutor salesDetailsJobExecutor;
	private WatchService watchService;

	@Autowired
	public DirWatcherService(SalesDetailsJobExecutor salesDetailsJobExecutor) {
		this.salesDetailsJobExecutor = salesDetailsJobExecutor;
	}

	@PostConstruct
	public void init() {
		try {
			this.watchService = FileSystems.getDefault().newWatchService();
			Path watchedDirPath = Paths.get(this.watchedDirPathValue);
			watchedDirPath.register(this.watchService, StandardWatchEventKinds.ENTRY_CREATE);
		} catch (IOException e) {
			throw new BeanCreationException("Problem while creating directory watcher", e);
		}
	}

	@Scheduled(fixedDelayString = "${scheduler.fixed.delay}")
	public void watchForNewFiles() {
		LOG.info(String.format("Watching for file changes in: %s", this.watchedDirPathValue));
		final WatchKey watchKey;

		try {
			watchKey = this.watchService.poll(this.watcherPollTimeout, TimeUnit.MINUTES);
			if (watchKey != null) {
				watchKey.pollEvents().stream().filter(e -> e.kind().equals(StandardWatchEventKinds.ENTRY_CREATE)).forEach(
						e -> {
							WatchEvent<Path> createdNewFileEvent = (WatchEvent<Path>) e;
							LOG.info(String.format("New file occurred: %s", createdNewFileEvent.context().getFileName()));
							try {
								this.salesDetailsJobExecutor.executeJob(
										this.watchedDirPathValue + createdNewFileEvent.context().getFileName().toString()
								);
							} catch (JobExecutionException ex) {
								LOG.warn("Problem while executing the job. Exception details: ", ex);
							}

							watchKey.reset();
						}
				);
			}
		} catch (InterruptedException e) {
			LOG.warn("Problem while polling for new files in watched directory", e);
		}
	}
}
