package pl.tramiaczek.filewatcher.boundary;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan({"pl.tramiaczek.filewatcher.control", "pl.tramiaczek.sales"})
public class SalesDetailsBatchProcessorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SalesDetailsBatchProcessorApplication.class, args);
	}
}
