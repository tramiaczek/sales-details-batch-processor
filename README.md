## Idea

The Spring Boot application monitors directory provided in **application.properties** file and if the righ CSV file occurs there the batch processing is started
and the file is parsed and written to database (HSQLDB on disk with Hikari Pool).

---

## Compiling and building

Project can be build using Maven as follows:

1. Run **mvn clean package** in the main directory.
2. The project will be build to the **target** dir.

---

## Configuration

There are to level of configuration: internal **application.properties** file that contains crucial configuration parameters. 
All of those parameters can be overrided by specifing another **application.properties** file under **config/** directory on the same level as build project 
(it is the Spring Boot mechanism for externalizing configuration). Especially two values could be switched in **config/application.properties** file:

1. watcher.directory.path=D:/TMP/test/ - the directory where new CSV file should arrive
2. processor.chunk.size=100 - commit-interval parameter of Spring Batch (how many lines read should execute write and commit of parsed entities)

Moreover the project can be run with special profile that will not write the entities to database (ex. to check memory usage during read). 
The Spring profile is: **only.csv.reading**

For usage with Postgres, the database server os required and new configuration file (
if default values should be overwritten) - **application-postgres.properties**. Moreover the application should be
executed with Spring profile: **postgres**. 

---

## CSV file to check

The file that matches the database schema can be downloaded from: http://eforexcel.com/wp/wp-content/uploads/2017/07/1500000%20Sales%20Records.zip
It is 1.5 millions sales records in CSV file. The application parses that object and tries to process them in batches.